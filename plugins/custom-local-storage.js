// Declare the local storage alternative class
function LocalStorageAlternative() {
  this.localStorageDictionary = {};
}
LocalStorageAlternative.prototype.setItem = function(key, value) {
  this.localStorageDictionary[key] = value;
}
LocalStorageAlternative.prototype.getItem = function(key) {
  return (this.localStorageDictionary[key] === 'undefined') ? null : this.localStorageDictionary[key];
}
LocalStorageAlternative.prototype.removeItem = function (key) {
  delete this.localStorageDictionary[key];
}

// Set the custom local storage
var customLocalStorage;
try {
  localStorage.setItem("zenestate-local-storage", "");
  localStorage.removeItem("zenestate-local-storage");
  customLocalStorage = localStorage;
} catch (error) {
  customLocalStorage = new LocalStorageAlternative();
}

export default customLocalStorage;

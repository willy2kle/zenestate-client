import axios from 'axios';
import customLocalStorage from '~/plugins/custom-local-storage';

// Create the HTTP client for ZenEstate
var axiosAirlisting;
export function getAxiosAirlisting (router) {
  // Return the HTTP client if it exists
  if (undefined !== axiosAirlisting)
    return axiosAirlisting;

  // Create and return the HTTP client
  var isLocalhost = (window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1');
  var apiUrl = isLocalhost ? 'http://localhost:8080/api' : 'http://api.zenestate.sg/api';
  axiosAirlisting = axios.create({
    baseURL: apiUrl
  });

  // Add an interceptor for redirect users with expired tokens
  axiosAirlisting.interceptors.response.use(
    function (response) {
      return response;
    }, function (error) {
      if ((undefined !== error.response) && (401 === error.response.status)) {
        customLocalStorage.removeItem('id_token');
        customLocalStorage.removeItem('profile');
        delete axiosAirlisting.defaults.headers.common['Authorization'];
        router.push('agentLogin');
      } else
        return Promise.reject(error);
    });

  return axiosAirlisting;
}

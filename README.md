## Introduction
This is the front-end code of [ZenEstate.sg](http://zenestate.sg). ZenEstate.sg is a Singapore-based real estate agent search engine.

## Technical details
### Nuxt.js
The front-end code of ZenEstate.sg is based on [Nuxt.js](https:/nuxtjs.io), which is a framework for server-rendered [Vue.js](https://vuejs.org/) applications. 

### Major modules
I am using [Auth0](https://auth0.com/) for the authentication, [Sentry](https://sentry.io/) for error logging, [Axios](https://github.com/axios/axios) to send HTTP requests and the UI is based on [Bootstrap](http://getbootstrap.com/) and [Element](http://element.eleme.io/#/en-US).

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

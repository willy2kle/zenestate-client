// Declare a mixin for agent facts
export default {
  // Methods
  methods: {
    // Convert a number into a shorter one
    toShortNumber: function(number) {
      // Return the number if it's smaller than 1000
      if (number < 1000)
        return number;

      // Add a "K" if the number is smaller than 10000
      if (number < 10000)
        return Math.round(number / 10) / 100 + "K";

      // Add a "K" if the number is smaller than 100000
      if (number < 100000)
        return Math.round(number / 100) / 10 + "K";

      // Add a "K" if the number is smaller than 100000
      if (number < 1000000)
        return Math.round(number / 1000) + "K";

      // Add a "M" if the number is smaller than 10000000
      if (number < 10000000)
        return Math.round(number / 10000) / 100 + "M";

      // Add a "M" if the number is smaller than 100000000
      if (number < 100000000)
        return Math.round(number / 100000) / 10 + "M";

      // Add a "M" otherwise
      return Math.round(number / 1000000) + "M";
    },

    // Convert a number of days into a shorter name
    toShortDuration: function(dayCount) {
      // Return the number of days if it's smaller than 5 weeks
      if (dayCount < 35)
        return dayCount + " days";

      // Return the number of weeks if it's smaller than 10 weeks
      if (dayCount < 70)
        return Math.round(dayCount / 7) + " weeks";

      // Return the number of months if it's smallar than 50 months
      if (dayCount < 1500)
        return Math.round(dayCount / 30) + " months";

      // Return the number of years otherwise
      return Math.round(dayCount / 365) + " years";
    },

    // Return the handled property types
    getHandledPropertyTypes: function(agentFact) {
      // Check if the facts of the agent have been downloaded
      var doesHandleCondos = agentFact['handles-condos'];
      var doesHandleHdb = agentFact['handles-hdb'];
      var doesHandleLandedHouses = agentFact['handles-landed-houses'];
      if ((undefined === doesHandleCondos) || (undefined === doesHandleHdb) ||
        (undefined === doesHandleLandedHouses))
        return "";

      // Retrieve the property types
      var propertyTypes = [];
      if (doesHandleCondos)
        propertyTypes.push("Condo");
      if (doesHandleHdb)
        propertyTypes.push("HDB");
      if (doesHandleLandedHouses)
        propertyTypes.push("Landed");

      // Check if no property types is handled
      if (0 === propertyTypes.length)
        return "None";

      // Return the property types
      return propertyTypes.join(", ");
    },

    // Return the regions covered
    getCoveredRegions: function(agentFact) {
      // Check that the regions have been defined
      if ((undefined === agentFact['covers-central']) || (undefined === agentFact['covers-east']) ||
        (undefined === agentFact['covers-west']) || (undefined === agentFact['covers-north']) ||
        (undefined === agentFact['covers-north-east']))
        return "";

      // Initialize the regions
      var regions = [];
      if (agentFact['covers-central'])
        regions.push('Central');
      if (agentFact['covers-east'])
        regions.push('East');
      if (agentFact['covers-west'])
        regions.push('West');
      if (agentFact['covers-north'])
        regions.push('North');
      if (agentFact['covers-north-east'])
        regions.push('North-East');

      // Check if no region is covered
      if (0 === regions.length)
        return "None";

      // Check if all the regions are covered
      if (5 === regions.length)
        return "All";

      // Return the regions
      return regions.join(', ');
    }
  }
}
